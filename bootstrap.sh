#!/usr/bin/env bash

echo ""
echo "      ___       ___          ___          ___                   ___      "
echo "     /  /\     /__/\        /  /\        /  /\         ___     /  /\     "
echo "    /  /::\    \  \:\      /  /:/_      /  /:/_       /  /\   /  /::\    "
echo "   /  /:/\:\    \  \:\    /  /:/ /\    /  /:/ /\     /  /:/  /  /:/\:\   "
echo "  /  /:/~/:/_____\__\:\  /  /:/_/::\  /  /:/ /::\   /  /:/  /  /:/~/:/   "
echo " /__/:/ /://__/::::::::\/__/:/__\/\:\/__/:/ /:/\:\ /  /::\ /__/:/ /:/___ "
echo " \  \:\/:/ \  \:\~~\~~\/\  \:\ /~~/:/\  \:\/:/~/://__/:/\:\\  \:\/:::::/ "
echo "  \  \::/   \  \:\  ~~~  \  \:\  /:/  \  \::/ /:/ \__\/  \:\\  \::/~~~~  "
echo "   \  \:\    \  \:\       \  \:\/:/    \__\/ /:/       \  \:\\  \:\      "
echo "    \  \:\    \  \:\       \  \::/       /__/:/         \__\/ \  \:\     "
echo "     \__\/     \__\/        \__\/        \__\/                 \__\/     "
echo ""
echo "        ..........................................................       "
echo "        . Dotfiles 0.1.6 (Pongstr) for setting up OSX Workspace  .       "
echo "        .      https://github.com/pongstr/dotfiles.git           .       "
echo "        ..........................................................       "
echo ""

# To run this, you must download & install the latest Xcode and Commandline Tools
# https://developer.apple.com/xcode/
# https://developer.apple.com/downloads/

[[ "$(uname -s)" != "Darwin" ]] && exit 0
  echo ""
  echo "  To run this, you must download & install the latest Xcode and Commandline Tools"
  echo "    > https://developer.apple.com/xcode/"
  echo "    > https://developer.apple.com/downloads/"
  echo ""
  echo "    ➜ Verifying Xcode Commandline Tools..."
  xcode-select --install
  echo ""
 
  # Function to check if a package exists
  check () { type -t "${@}" > /dev/null 2>&1; }


# Install Homebrew
# ---------------------------------------------------------------------------
echo ""
echo "Checking if Homebrew is installed..."
echo ""

if check brew; then
  echo "Awesome! Homebrew is installed! Now updating..."
  echo ""
  brew upgrade
  brew update
  echo ""
  echo "     setting Ansible up through Homebrew packages..."

  echo ""
  echo "     ➜ installing python..."
  brew install python
    
  echo ""
  echo "     ➜ installing ansible..."
  brew install ansible
fi

if ! check brew; then
  echo "    Download and install homebrew"
  echo ""
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

  # Run Brew doctor before anything else
  brew doctor
  echo ""
  echo "     Homebrew is ready..."

  echo ""
  echo "     setting Ansible up through Homebrew packages..."

  echo ""
  echo "     ➜ installing python..."
  brew install python
    
  echo ""
  echo "     ➜ installing ansible..."
  brew install ansible
fi

  
echo "Installing Caskroom, Caskroom versions and Caskroom Fonts..."
brew tap caskroom/homebrew-cask
install brew-cask
brew tap caskroom/versions
# Make /Applications the default location of apps
echo ""
echo "  ➜ Setting up HOMEBREW_CASK_OPTS's path..."
export HOMEBREW_CASK_OPTS="--appdir=/Applications --caskroom=/opt/homebrew-cask/caskroom"
echo ""
# echo "  ➜ installing GPG Tools..."
# brew cask install gpgtools
# brew cask cleanup
# brew cleanup


echo ""
echo "      ➜ git cloning dotsh into /tmp/dotsh..."
git clone https://gitlab.com/baseboxorg/osx-boxup-bootstraps.git /tmp/dotsh
cd /tmp/dotsh

echo ""
echo "  ➜ Setting up dotsh..."
ansible-playbook \
  --diff \
  -v \
  --ask-vault-pass \
  -i /tmp/dotsh/dotfiles/inventory \
  /tmp/dotsh/dotfiles/osx.yml

